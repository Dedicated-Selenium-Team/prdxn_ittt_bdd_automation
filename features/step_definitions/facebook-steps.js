'use strict';

var assert=require('assert');

module.exports = function() {
  this.World = require('../support/world.js').World;

  this.Given(/^Open Chrome and Start Application$/, function () {
    this.driver.get('http://www.facebook.com');
  });

  this.Then(/^Verify the page title \- "([^"]*)"$/, function(title) {
    this.driver.getTitle().then(function(mytitle){
      assert.equal(mytitle,title);
    });
  });   

  this.Then(/^Check size of font$/, function() {
    this.driver.findElement({ css:'._5iyx'}).then(function(div){
      var fSize=div.getCssValue('font-size').then(function(size){
        console.log(size);
      });
    });
  });

  // this.Then(/^Get PageSource$/,function(){
  //   this.driver.getPageSource().then(function(pageSource){
  //     console.log(pageSource);
  //   });
  // });

  // this.Then(/^Count anchor tag$/,function () {
  //   this.driver.findElements({ css: 'a' }).then(function(anchors){
  //     for(var i=0;i<anchors.length;i++){
  //       var values=anchors[i].getText().then(function(text){
  //         console.log("Text: "+text);
  //       }); 
  //     }
  //   });
  // });

  // this.When(/^I enter valid \"([^\"]*)\" and \"([^\"]*)\"$/, function (uname,pass) {
  //   this.driver.findElement({ id: 'email' }).sendKeys(uname);
  //   this.driver.findElement({ id: 'pass' }).sendKeys(pass);
  // });

  // this.Then(/^user should be able to login successfully$/, function () {
  //   this.driver.findElement({ id: 'u_0_l' }).click();
  // });

};
