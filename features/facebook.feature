Feature: Test Facebook smoke scenario

Scenario Outline: Test login with valid credentials

Given Open Chrome and Start Application
Then Verify the page title - "<title>"
Then Check size of font
# Then Get PageSource
# Then Count anchor tag
# When I enter valid "<username>" and "<password>"
# Then user should be able to login successfully

Examples: 
| username | password | title                        |
| abcd1    | abcd1    | Facebook - Log In or Sign Up |
