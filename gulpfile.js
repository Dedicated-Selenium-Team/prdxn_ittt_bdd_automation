var gulp=require('gulp');
var cucumber=require('gulp-cucumber');

gulp.task('cucumber',function(){
	return gulp.src('*features/*').pipe(cucumber({
		'steps': '*features/step_definitions/*.js',
		'support': '*features/support/*.js',
		'format': 'pretty'
	}));
});

// gulp.task('watch',function(){
// 	gulp.watch('*features/**/*.{js,feature}',['cucumber']);
// });

gulp.task('default',['cucumber']);